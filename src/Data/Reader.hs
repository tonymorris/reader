{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Data.Reader( module R ) where

import Data.Reader.Readers as R
import Data.Reader.Reader1 as R
import Data.Reader.Reader2 as R
import Data.Reader.Reader3 as R

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Data.Reader.Readers(
  _Reader1Reader2
, _Reader2Reader1
) where

import Control.Lens ( iso, Iso )
import Data.Reader.Reader1 ( Reader1T(..) )
import Data.Reader.Reader2 ( Reader2T(..) )
import Data.Reader.Reader3 ( Reader3T(..) )

_Reader1Reader2 ::
  Iso
    (Reader2T f a b)
    (Reader2T f' a' b')
    (Reader1T a f b)
    (Reader1T a' f' b')
_Reader1Reader2 =
  iso
    (\(Reader2T f) -> Reader1T f)
    (\(Reader1T f) -> Reader2T f)

_Reader1Reader3 ::
  Iso
    (Reader3T f b a)
    (Reader3T f' b' a')
    (Reader1T a f b)
    (Reader1T a' f' b')
_Reader1Reader3 =
  iso
    (\(Reader3T f) -> Reader1T f)
    (\(Reader1T f) -> Reader3T f)

_Reader2Reader1 ::
  Iso
    (Reader1T a f b)
    (Reader1T a' f' b')
    (Reader2T f a b)
    (Reader2T f' a' b')
_Reader2Reader1 =
  iso
    (\(Reader1T f) -> Reader2T f)
    (\(Reader2T f) -> Reader1T f)

_Reader2Reader3 ::
  Iso
    (Reader3T f b a)
    (Reader3T f' b' a')
    (Reader2T f a b)
    (Reader2T f' a' b')
_Reader2Reader3 =
  iso
    (\(Reader3T f) -> Reader2T f)
    (\(Reader2T f) -> Reader3T f)

_Reader3Reader1 ::
  Iso
    (Reader1T a f b)
    (Reader1T a' f' b')
    (Reader3T f b a)
    (Reader3T f' b' a')
_Reader3Reader1 =
  iso
    (\(Reader1T f) -> Reader3T f)
    (\(Reader3T f) -> Reader1T f)

_Reader3Reader2 ::
  Iso
    (Reader2T f a b)
    (Reader2T f' a' b')
    (Reader3T f b a)
    (Reader3T f' b' a')
_Reader3Reader2 =
  iso
    (\(Reader2T f) -> Reader3T f)
    (\(Reader3T f) -> Reader2T f)
